const fs = require('fs');

const assert = require('assert');
const { rollup } = require('rollup');
const nanoPlugin = require('../');
import { nano } from './fixtures/nano-def';
// let nano;
process.chdir(__dirname);

const inputName = 'input.js';
const outputName = 'output.js';
const outputStylesName = 'generatedStyles.css';
const fixtureStylesName = 'output.css';

function getPath(input) {
	return './fixtures/templates/' + input + '/';
}

function bundle(input, nano) {
	const path = getPath(input);
	return rollup({
		input: path + inputName,

		plugins: [
			nanoPlugin({
				include: ['**/input.js', '**/styles.js'],
				nano: nano,
				outputFile: path + outputStylesName
			})
		]
	}).then(bundle => {
		return bundle.write( {
			format: 'es',
			file: 'bundle.js',
		});
	});
}

const tests = {
	// 'nested-imports': 'should work with nested imports',
	simple: 'should convert nano calls to object literals and output a css file',
	// 'use-rule': 'should leave imports alone when used with methods other than "sheet"',
	// variables: 'should handle constants in rules',
	// 'multiple-variables': 'should handle multiple of the same constants in rules',
	// 'import-variables': 'should handle imports for rules',
	// 'different-name': 'should handle when nano is imported using a different name',
	// 'nested-variables': 'should handle nested variables',
	// 'supports-require': 'should handle require() syntax',
};

describe('rollup-plugin-nano-css', () => {

	// beforeEach(() => {
	// 	({nano} = require('./fixtures/nano-def'));
	// });

	const testNames = Object.keys(tests);
	testNames.forEach(name => {
		it(tests[name], () => {
			const path = getPath(name);
			return bundle(name, nano).then((result) => {
				const code = result.output[0].code;
				const output = fs.readFileSync(path + outputName);
				assert.strictEqual(code, output.toString());
				const stylesheetFixture = fs.readFileSync(path + fixtureStylesName);
				const generatedStylesheet = fs.readFileSync(path + outputStylesName);
				assert.strictEqual(generatedStylesheet.toString(), stylesheetFixture.toString());
			});
		});
	});

	afterEach(() => {
		if (fs.existsSync('./styles.css')) {
			fs.unlinkSync('./styles.css');
		}

		if (fs.existsSync('./bundle.js')) {
			fs.unlinkSync('./bundle.js');
		}
	});

	after(() => {
		const testNames = Object.keys(tests);
		testNames.forEach(name => {
			const path = getPath(name) + outputStylesName;
			if (fs.existsSync(path)) {
				fs.unlinkSync(path);
			}
		});
	});
});
