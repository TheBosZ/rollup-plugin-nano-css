import { nano } from '../../nano-def';
import Colors from './Colors';

const metricStyles = {
	color: Colors.$filter_anchor,
	'font-size': '1rem',
	'letter-spacing': '.2px',
};

export default nano.sheet({
	icon: metricStyles,
});
