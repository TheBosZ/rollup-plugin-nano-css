const { nano } = require('../../nano-def');

const result = nano.sheet({
	input: {
		border: '1px solid grey',
	},
	button: {
		border: '1px solid red',
		color: 'red',
	}
});
const str = result.button;
module.exports = str;
