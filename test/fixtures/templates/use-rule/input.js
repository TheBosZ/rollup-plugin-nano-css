import { nano } from '../../nano-def';

const result = nano.sheet({
	input: {
		border: '1px solid grey',
	},
	button: {
		border: '1px solid red',
		color: 'red',
	}
});

const result2 = nano.rule({
	color: 'blue',
});

export default {button: result.button, bob: result2};
