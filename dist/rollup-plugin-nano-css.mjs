import { createFilter } from 'rollup-pluginutils';
import MagicString from 'magic-string';
import { addon } from 'nano-css/addon/extract';
import { walk } from 'estree-walker';
import { writeFileSync } from 'fs';
import { ensureFileSync } from 'fs-extra';

function nanoCSS(opts) {
	if ( opts === void 0 ) opts = {};


	var filter = createFilter(opts.include, opts.exclude);

	if (!opts.nano) {
		throw new Error('must specify nano');
	}

	var nano = opts.nano;
	addon(nano);

	function finishedGenerate(bundleOpts, bundle, isWrite) {
		if (opts.outputFile && (typeof isWrite === 'undefined' || isWrite)) {
			ensureFileSync(opts.outputFile);
			writeFileSync(opts.outputFile, nano.raw);
		}
	}

	return {
		name: 'nano-css',

		transform: function (code, id) {
			if (!filter(id)) {
				return null;
			}

			var that = this;

			var canRemoveImport = true;

			var magicString = new MagicString(code);
			var result = that.parse(code);

			var importStart = null, importEnd = null, importName = 'nano', foundImport = false;
			var isNano = false;

			walk(result, {
				enter: function enter(node) {
					//Find the import declarations for nano and keep track of it
					var walkThat = this;
					if (!node || !node.type) {
						return;
					}

					if (node.type === 'ImportDeclaration' && node.specifiers && !foundImport) {

						var specs = node.specifiers;
						specs.forEach(function (spec) {
							if ((spec.imported && spec.imported.name === 'nano') ||
								(spec.local && spec.local.name === 'nano')
							) {
								importStart = node.start;
								importEnd = node.end;
								importName = spec.local.name;
								isNano = true;
								foundImport = true;
							}
						});
					}

					if (node.type === 'VariableDeclaration' && node.declarations && node.declarations.length && !foundImport) {
						//Check to see if this declaration is the require('nano'); we're looking for
						var decs = node.declarations;
						decs.forEach(function (dec) {
							if (dec.id) {
								//They might use const nano = require('./nano');
								if (
									dec.id.type === 'Identifier' &&
									dec.id.name === 'nano' &&
									!foundImport
								) {
									foundImport = true;
									importStart = node.start;
									importEnd = node.end;
									importName = dec.id.name;
									isNano = true;
									foundImport = true;
								}

								//They've might use: const { nano } = require('./nano');
								if (
									dec.id.type === 'ObjectPattern' &&
									dec.id.properties &&
									dec.id.properties.length &&
									!foundImport
								) {
									var props = dec.id.properties;
									props.forEach(function (prop) {
										if (prop.key && prop.key.name === 'nano') {
											foundImport = true;
											importStart = node.start;
											importEnd = node.end;
											importName = prop.key.name;
											isNano = true;
											foundImport = true;
										}
									});
								}
							}
						});
					}

					//Find any calls to nano.sheet
					if (node.type === 'CallExpression' && node.callee && node.callee.type === 'MemberExpression') {
						var callee = node.callee;
						if (callee.object.name === importName) {
							//If the call was to something *other* than nano.sheet or nano.rule or nano.put or nano.global, we can't remove the import
							if (
								callee.property.name !== 'sheet' &&
								callee.property.name !== 'rule' &&
								callee.property.name !== 'put' &&
								callee.property.name !== 'global'
							) {
								canRemoveImport = false;
							}

							if (callee.property.name === 'sheet' && node.arguments) {
								var args = node.arguments;
								var x;
								if (args.length) {
									try {
										x = require(id);
									} catch (ex) {
										that.warn('Failed to simplify nano call');
										that.warn('Failed to load file: ' + ex.toString());
										canRemoveImport = false;
									}

									if (x && canRemoveImport) {


										try {
											var replacedString = x.default ? JSON.stringify(x.default) : JSON.stringify(x);
											if (replacedString.length < 3) {
												that.warn('Replacement string is not long enough, not going to overwrite');
												canRemoveImport = false;
											} else {
												magicString.addSourcemapLocation(0);
												magicString.addSourcemapLocation(code.length);
												magicString.overwrite(0, code.length, replacedString);
												walkThat.skip(); //Finish going through the tree, we're done
											}

										} catch (ex) {
											that.warn('Failed to simplify nano call: ' + ex.toString());
											canRemoveImport = false;
										}
									}
								}
							}
						}
					}
				}
			});
			if (isNano && canRemoveImport) {
				return {
					code: ("export default " + (magicString.toString()) + ";"),
					map: magicString.generateMap(),
				};
			}
			return code;

		},
		generateBundle: finishedGenerate

	};
}

export default nanoCSS;
