# Changelog

## 0.2.1

Recognize `.global()` calls. Better way to put "reset" or "base" styles. They can just be removed.

## 0.2.0 (Breaking change)

Update to stop Rollup deprecation warning. That means you'll have to use [Rollup v1.0.0](https://github.com/rollup/rollup/blob/f5a6c3008a92716bae01109e944ed9f4214557c3/CHANGELOG.md#100) or above 

## 0.1.2

Recognize `.put()` calls. They kinda almost work the same as `.sheet()`. They're useful to put "reset" or "base" styles in.

## 0.1.1

Properly recognize `.rule()` calls. They work the same as `.sheet()` and so should be allowed.

## 0.1.0

Better tree-shaking by replacing Nano file with result of the `require` call

## 0.0.4

Found a situation where the resulting style object gets tree-shaken to nothing. Workaround to try to prevent that.

## 0.0.3

Initial release
